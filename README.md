# Aria 2
Your personnal open-source cross-platform assistant.

## Features.
* Register/Login system for multiple users.
* Create your own modules.

## Notes of the version.
**Status :** _Alpha_
**Version :** _alpha-0.6_
**Core version :** _0.2_
**Last update:** _18/05/2017/12:00_

### Summary :
Light corrections of the bugs.
5 modules added.

### Detailed notes :
* Correction of definition regex (french).
* Added multiple definitions if such exist.
* You can now ask aria what modules are loaded.
* Added module "choice" : returns "one" or "two" when you ask Aria "one or two".
* Added module "recipes" (en only) : "recipes with *".
* Added module "localization (fr only) : "code postal du/de *". (API du gouvernement).
* Added module "medicament" (fr only) : "informations médicament *" (API du gouvernement).
* Added module "fiscentreprise" (fr only) : "informations employés" => calculez le coût d'un employé (API du gouvernement).
* Activated modules choice, recipes, localization, medicament and fiscentreprise.

### ToDo :
* Convert numbers written with letters to figures. (synonyms).
* Analyze the sentence to extract terms to find calories (french).
* Show the colors (french).
* Data sent when interacting need to be stored in the user profile. This will allow to use Aria with any API (Aria inCloud).
* Recipe module.
* You can reload the page by asking Aria to do it. (Might be useful sometime, maybe...).
* Learning Mode : talk to Aria and tell her what she has potentially to answer.
    Automatic detection of humor and duplicates.
    When Aria will search for answers she won't find, she'll check on the "Learning Mode Answers" file.

## Built-in modules.
* Base module.
* Economy module.

## Languages for the **base & economy** modules.
- English
- French

## Next big updates aim :
* Add the option to encrypt user data (user data in its profile, "infos" input) via API call of FLBW encryption algorithm.
* Create the API part for developers messages to be shown to the user in the Human-Aria interface (me/index.php).
* Create the cache management system.
* Aria inCloud : store your user data on the cloud and access it from your computer thanks to the API.
* Meeting module : choose Aria to be in charge of receiving your meeting demands.

## Module developers :
* Amael Cattaneo
* Andy Malgonne
* Flavien Berwick

### External modules :
* Choice (en) (Andy Malgonne)
* Fiscentreprise (fr) (Amael Cattaneo)
* Localization (fr) (Amael Cattaneo)
* Medicament (fr) (Amael Cattaneo)
* Recipes (en) (Amael Cattaneo)