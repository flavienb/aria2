<?php

/*
 * Aria is an Open Source project.
 * Aria's core. Manage the modules.
 */

session_start();
include("../assets/functions.php");

$new_token = md5(time());
$data = array(
    "text" => "It seems like I don't have that answer. Sorry. No module answered.",
    "request_answer" => "no",
    "infos" => "",
    "resources_to_load" => "",
    "is_allowed_to_ask" => "yes",
    "new_token" => $new_token
);
$langs = array(
    "en",
    "fr"
);
$sentences_lnf = array(
    "I haven't found the language asked.",
    "This language does not exist yet.",
    "Well, this language is not ready yet.",
    "Please select an available language... Like English!"
);

if (true) { //IsAllowedToAsk() will be added later.
    if (isset($_POST["token"]) && isset($_POST["text"]) && isset($_POST["infos"])) {
        $query = htmlentities($_POST["text"]);
        $token = htmlentities($_POST["token"]);
        $token_to_check = md5($token . getIp());
        $infos = json_decode($_POST["infos"], true);
        $token_sess = (isset($_SESSION["token_aria"])) ? $_SESSION["token_aria"] : "";
        $lang = (isset($_POST["lang"]) && !empty($_POST["lang"])) ? htmlentities($_POST["lang"]) : "en";
        if ($token_to_check == $token_sess) {
            require("../modules/modules.php");
            if (!empty($infos) && $infos["request_answer"] == "yes" && isset($infos["module_name"]) && $infos["module_name"] != "" && isset($infos["function_name"]) && $infos["function_name"] != "") {
                /* Interaction */
                $module = ucfirst(htmlentities($infos["module_name"]));
                $function_name = htmlentities($infos["function_name"]);
                if (in_array(strtolower($module), $enabled_modules)) {
                    if (in_array($lang, $langs)) {
                        require("../modules/" . strtolower($module) . "/base.class.php");
                        if (method_exists($module, $function_name)) {
                            $regexes = getDataFromJson("../modules/" . $infos["module_name"] . "/langs/regex." . $lang . ".json");
                            for ($i = 0; $i < sizeof($regexes); $i++) {
                                if ($regexes[$i]["options"]["function_to_call"] == $function_name) {
                                    break;
                                }
                            }
                            $others = (isset($regexes[$i]["options"]["others"])) ? $regexes[$i]["options"]["others"] : false;
                            $process = new $module($language = $lang, $query, $others, $function_name, $infos);
                            $returned_data = $process->getAnswer();
                            /* Merge the returned data of the module with the default data array */
                            $data = array_merge($data, $returned_data);
                        } else {
                            $data["text"] = "Function in module unreachable.";
                        }
                    } else {
                        $data["text"] = ($sentences_lnf[rand(0, sizeof($sentences_lnf) - 1)]);
                    }
                } else {
                    $data["text"] = "Module unreachable.";
                }
            } else {
                /* First question */
                $matched = false;
                foreach ($enabled_modules as $module) {
                    /* Looking for preg_match on each module */
                    if (in_array($lang, $langs) && file_exists("../modules/$module/langs/regex.$lang.json")) {
                        $act_mod_infos = getDataFromJson("../modules/" . $module . "/langs/regex." . $lang . ".json"); // Actual Module Infos (base.json).
                        for ($i = 0; $i < sizeof($act_mod_infos); $i++) {
                            $pattern = $act_mod_infos[$i]["pattern"];
                            $act_mod_infos[$i]["insensitive"]=(isset($act_mod_infos[$i]["insensitive"]))?$act_mod_infos[$i]["insensitive"]:"yes";
                            if ($act_mod_infos[$i]["insensitive"] == "yes") {
                                /* Check if the pattern needs to be case sensitive */
                                $flags = "i";
                            } else {
                                $flags = "";
                            }
                            if (@preg_match("#" . $pattern . "#" . $flags, html_entity_decode($query))) {
                                $matched = true;
                                break;
                            }
                        }
                        if ($matched) {
                            break;
                        }
                    } else {
                        $data["text"] = ($sentences_lnf[rand(0, sizeof($sentences_lnf) - 1)]);
                    }
                }
                if ($matched) {
                    /* Process the module */
                    require("../modules/" . $module . "/" . "base.class.php");
                    $module = ucfirst($module);
                    $others = isset($act_mod_infos[$i]["options"]["others"]) ? $act_mod_infos[$i]["options"]["others"] : "";
                    $process = new $module($language = $lang, $query, $others, $act_mod_infos[$i]["options"]["function_to_call"]);
                    $returned_data = $process->getAnswer();
                    /* Merge the returned data of the module with the default data array */
                    $data = array_merge($data, $returned_data);
                } else {
                    /* No module got the query matching their pattern */
                    include("../modules/base/base.class.php"); // Loading default base module.
                    $base = new Base($language = $lang, $query);
                    $unkw = $base->getUnknownAnswer();
                    if (is_array($unkw)) {
                        $data["text"] = $unkw[0];
                        $data["text_important"] = $unkw[1];
                        // DONT WORK
                    } else {
                        $data["text"] = $unkw;
                    }
                }
            }
        } else {
            $data["text"] = "Token error. Please try again.";
        }
        $_SESSION["token_aria"] = md5($new_token . getIp());
    } else {
        $data["text"] = "It seems like you haven't given me enough informations. Please fill all the field or try to reload the page.";
    }
    $data["lang"] = $lang;
} else {
    $data["is_allowed_to_ask"] = "no";
}

echo json_encode($data);
?>