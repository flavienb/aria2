﻿<?php
/*
 * Aria is an Open Source project.
 * Human to Aria interface.
 */

include("../assets/header.php");
if (!isset($_SESSION["user"])) {
    header("Location:../");
}
$token = md5(time());
$_SESSION["token_aria"] = md5($token . getIp());
?>
<script>
    function changeLang(lang = "", speed = "1") {
        artyom.fatality();
        console.log("Choosen : " + lang);
        if (lang === "") {
            lang = $('input[name=lang]:checked').val();
        }
        if (lang === "fr") {
            lang = "fr-FR";
            $('#fr').prop("checked", true);
        } else {
            lang = "en-GB";
            $('#en').prop("checked", true);
        }
        setTimeout(function () {
            artyom.initialize({
                lang: lang,
                continuous: true,
                soundex: true,
                debug: true,
                listen: true,
                speed: speed
            }).then(() => {
                console.log("Atryom started.");
            }).catch((err) => {
                console.error("Artyom couldn't be initialized: ", err);
            });
        }, 250);
    }
</script>
<div class="label-version">Version alpha-0.5.1</div>
<div class="container">
    <div class="row">
        <div class="offset-lg-2 col-lg-8 offset-lg-2">
            <center>
                <div class="view">  
                    <div class="plane main">    
                        <div class="circle"></div>    
                        <div class="circle"></div>    
                        <div class="circle"></div>    
                        <div class="circle"></div>    
                        <div class="circle"></div>    
                        <div class="circle"></div>  
                    </div>
                </div>
                <form method="POST" action="" id="aria">
                    <div class="form-group">
                        <input type="text" class="form-control aria-textbox speech-input" name="text" autocomplete="off" autofocus/>
                        <input type="hidden" name="token" value="<?php echo $token; ?>"/>
                        <input type="hidden" name="infos" value=""/>
                    </div>
                    <div class="form-group" onclick="changeLang()">
                        <input type="radio" name="lang" id="en" value="en" checked> English
                        <input type="radio" name="lang" id="fr" value="fr"> Français
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" class="form-control" name="sbm">Send</button>
                    </div>
                </form>
                <div class="panel panel-default" id="answer_tb">

                </div>
                
            </center>
        </div>
    </div>
</div>
<?php
$more = "<script src=\"../assets/js/aria.js\"></script>";
$more .= "<div id='additional_resources'></div>";
include("../assets/footer.php");
?>