<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Recipes extends Module{
    /* Please DO NOT remove the __construct(); code to avoid errors. */

    private $_aliment,$_recipesArray, $_recipes, $_matches, $_idMenu;

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "recipes"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        //Manage the call of request function to call
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }

     //api identifiant dd98c102
     //api keys 30de1261d5b510c0105a06375775323a

	 public function GetAllRecipes()
	 {
		$this->SetAliment();
		$this->SetRecipes();
		$this->SetHtmlRecipes();
		$this->GetRecipes();
	 }
	 
     public function SetAliment()
     {
       preg_match('#with (\w+)#', $this->user_query, $this->_matches);
       $this->_aliment=$this->_matches[1];
     }

     public function SetRecipes()
     {
		$this->_recipesArray = json_decode(curlPost("https://api.edamam.com/search?q='".$this->_aliment."'&app_id=dd98c102&app_key=30de1261d5b510c0105a06375775323a", array())['response'],true);
     }

     public function SetHtmlRecipes()
     {
       $this->_recipes="";
	   $this->_idMenu=0;
	   
       foreach ($this->_recipesArray['hits'] as $recette) {
		   $listeIngredients="";
		   foreach ($recette['recipe']['ingredients'] as $ingredients) {	
			   $listeIngredients.="<li>".$ingredients['text']."</li>";
		   }
           $this->_recipes.='<div><img src="'.$recette['recipe']['image'].'">
		   <div class="dropdown" class="recipes">
				<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				'.$recette['recipe']['label'].'
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$this->_idMenu.'">
				<li><a href="'.$recette['recipe']['url'].'" title="Link to see the recipe">Link to see the recipe</a></li>
				<li role="separator" class="divider"></li>
				<li class="dropdown-header"><h4>Aliment</h4></li>
				'.$listeIngredients.'
				</ul>
				</div>';
		   $this->_idMenu++;
       }
     }

     public function GetRecipes()
     {
       $this->setText($this->_recipes);
	   $this->setResourcesToLoad("../modules/recipes/css/resource1.css");
     }

}