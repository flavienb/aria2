<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Medicament extends Module {
    /* Please DO NOT remove the __construct(); code to avoid errors. */

	private $_listeMedicament, $_idMedicament, $_tableau, $_request, $_matches, $_curl, $_surveillance, $_remboursement, $_composition;
	
    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "medicament"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }
	 
	 private function getTableau($request){
		 
		 /*Initialisation de la ressource curl*/
		$this->_curl = curl_init();
		/*On indique à curl quelle url on souhaite télécharger*/
		curl_setopt($this->_curl, CURLOPT_URL, "https://medicaments.api.gouv.fr/api/medicaments?nom=".$request);
		/*On indique à curl de nous retourner le contenu de la requête plutôt que de l'afficher*/
		curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
		/*On indique à curl de ne pas retourner les headers http de la réponse dans la chaine de retour*/
		curl_setopt($this->_curl, CURLOPT_HEADER, false);
		/*On execute la requete*/
		$this->_tableau = json_decode(curl_exec($this->_curl),true);
		/*On ferme la ressource*/ 
		curl_close($this->_curl);
	 }
	 
	 private function getMedicamentListe(){
		$this->_listeMedicament="";
	    $this->_idMedicament=0;
	   
       foreach ($this->_tableau as $listeMedicaments) {
		   $listePresentation="";
		   
		   foreach ($listeMedicaments['presentation'] as $presentation) {
				
				if($presentation['tauxRemboursement'][0]===""){
					$this->_remboursement = "0%";
				   }else
				   {
					   $this->_remboursement = $presentation['tauxRemboursement'][0];
				   }
		   
			   $listePresentation.="<li>vendu par ".$presentation['libelle']." remboursé à ".$this->_remboursement."</li>";
		   }
		   
		   foreach ($listeMedicaments['voiesAdmnistration'] as $voies) {
		   
			   $listePresentation.="<li>administrable par voie ".$voies."</li>";
		   }
		   
		   if($listeMedicaments["surveillanceRenforcee"]){
			   $this->_surveillance = "oui";
		   }else
		   {
			   $this->_surveillance = "non";
		   }
		   
		   foreach ($listeMedicaments['composition'] as $composition) {
			   $this->_composition=$composition["denominationSubstance"];
			   $listePresentation.="<li>composé de ".$this->_composition."</li>";
		   }
		   
           $this->_listeMedicament.='<div>
		   <div class="dropdown" class="city">
				<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				'.$listeMedicaments["nom"].'
				<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$this->_idMedicament.'>
				<li>'.$listeMedicaments["formePharmaceutique"].'</li>
				<li role="separator" class="divider"></li>
				<li>'.$listeMedicaments["etatCommercialisation"].' depuis le '.$listeMedicaments["dateMiseSurLeMarche"].'</li>
				<li role="separator" class="divider"></li>
				<li>titulaire de '.$listeMedicaments["titulaire"][0].'</li>
				<li role="separator" class="divider"></li>
				<li>surveillé: '.$this->_surveillance.'</li>
				<li role="separator" class="divider"></li>
				'.$listePresentation.'
				<li role="separator" class="divider"></li>
				</ul>
				</div>';
		   $this->_idMedicament++;
       }
	   
	   $this->setText($this->_listeMedicament);
	 }
	 
	 private function getMedicaments(){
		 preg_match('#information(s?) ((sur )?)m(e|é)dicament(s?) (\w+)#', $this->user_query, $this->_matches);
		 $this->getTableau($this->_matches[6]);
		 $this->getMedicamentListe();
	 }
}
