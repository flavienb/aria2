<?php

/*
 * Aria is an Open Source project.
 * Contains the name of all the modules that are enabled and that will be browsed by the core of Aria.
 */

$enabled_modules = array(
    "base",
    "economy",
    "choice",
    "localization",
    "medicament",
    "recipes",
    "fiscentreprise"
);
?>