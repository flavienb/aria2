<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Equation extends Module {

    private $_toConvert, $_binaire, $_baseEntree, $_baseSortie, $_convertBin, $_convert, $_reste;

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "equation"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }

    private function getText() {
        $this->setText("SUCCESS and additional infos are : "); // Set the text to show.
        $this->setText($this->additional_infos["first_additional_info"]); // Show what is inside the ["others"] array in langs/regex.xx.json.
    }

    private function conversionBinaire($_toConvert, $_baseEntree) {
        $_toconvert = $_reste;
        while ($_toConvert > $_baseEntree) {
            $_binaire = $_reste % $_baseEntree;
            $_convertBin .= $_binaire;
            $_reste = ($_reste - $_binaire) / $_baseEntree;
        }
        $_convertBin .= $_reste;
        conversion($_toConvert, $_baseSortie);
    }

    private function conversion($_toConvert, $_baseSortie) {
        if ($_baseSortie === "2" || $_baseSortie === "binaire" || $_baseSortie === "binary") {
            $_convert = strrev($convertBin);
        } elseif ($_baseSorti === "16" || $_baseSortie === "hexa" || $_baseSortie === "hexadecimal" || $_baseSortie === "hexadécimal") {
            for ($i = 0; strlen($_convert); $i++) {
                echo $i;
            }
        } elseif ($_baseSorti === "10" || $_baseSortie === "deci" || $_baseSortie === "decimal" || $_baseSortie === "hexadécimal") {
            
        } else {
            
        }
        $this->setText("Votre valeur " + $_toconvert + " en base " + $_baseEntree + " est égale a " + $_convert + " en base " + $_baseSortie);
    }

    private function verification() {
        if (!preg_match("#[^01]#", $_toConvert) && ($_baseEntree == "2" || $_baseEntree === "binaire" || $_baseEntree === "binary" )) {
            $_toConvert = $_convertBin;
            conversion($_toConvert, $_baseSortie);
        } elseif (!preg_match("#[^0-9]#", $_toConvert) && ($_baseEntree === "10" || $_baseEntree === "deci" || $_baseEntree === "decimal" || $_baseEntree === "hexadécimal")) {
            conersionBinaire($_toConvert);
        } elseif (!preg_match("#[^0-9A-F]#", $_toConvert) && ($_baseEntree === "16" || $_baseEntree === "hexa" || $_baseEntree === "hexadecimal" || $_baseEntree === "hexadécimal")) {
            conersionBinaire($_toConvert);
        } else {
            $this->setText("Votre entrée n'est pas correcte, veuiller vérifier la bonne saisie des valeurs");
        }
    }

}
