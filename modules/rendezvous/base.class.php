<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

/* DO NOT FORGET TO RENAME THIS MODULE ONCE YOU'VE COPY-PASTED IT (change 'class Example' to 'class MyClassName') */
class RendezVous extends Module {
    /* Please DO NOT remove the __construct(); code to avoid errors. */

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "rendezvous"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }
    
    private function getText() {
        $this->setText("SUCCESS and additional infos are : "); // Set the text to show.
        $this->setText($this->additional_infos["first_additional_info"]); // Show what is inside the ["others"] array in langs/regex.xx.json.
    }

}
