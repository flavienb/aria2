	<?php

	if (file_exists("../../assets/functions.php")) {
		require("../../assets/functions.php");
	}
	require(correctPathFichier("modules/module.class.php"));

	class Localization extends Module {
		/* Please DO NOT remove the __construct(); code to avoid errors. */

		private $_codePostal, $_city, $_idCity, $_tableau, $_request, $_matches, $_curl;
		
		function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
			$this->module_name = "localization"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
			$args = func_get_args();
			call_user_func_array(array($this, 'parent::__construct'), $args);
			/* Manage the call of request function to call */
			if ($function_to_call != "") {
				$this->$function_to_call();
			}
		}

	   
		 private function getTableau($request){
			 
			 /*Initialisation de la ressource curl*/
			$this->_curl = curl_init();
			/*On indique à curl quelle url on souhaite télécharger*/
			curl_setopt($this->_curl, CURLOPT_URL, "https://geo.api.gouv.fr/communes?".$request."&fields=nom,code,codesPostaux,codeDepartement,codeRegion,population&format=json&geometry=centre");
			/*On indique à curl de nous retourner le contenu de la requête plutôt que de l'afficher*/
			curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
			/*On indique à curl de ne pas retourner les headers http de la réponse dans la chaine de retour*/
			curl_setopt($this->_curl, CURLOPT_HEADER, false);
			/*On execute la requete*/
			$this->_tableau = json_decode(curl_exec($this->_curl),true);
			/*On ferme la ressource*/ 
			curl_close($this->_curl);
		 }
		 
		 private function getVilleText(){
			$this->_city="";
			$this->_idCity=0;
		   
		   foreach ($this->_tableau as $listeVilles) {
			   $listeCodePostaux="";
			   foreach ($listeVilles['codesPostaux'] as $codesPostaux) {	
				   $listeCodePostaux.="<li>Code postal: ".$codesPostaux."</li>";
			   }
			   $this->_city.='<div>
			   <div class="dropdown" class="city">
					<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					'.$listeVilles["nom"].'
					<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu'.$this->_idCity.'>
					<li role="separator" class="divider"></li>
					'.$listeCodePostaux.'
					<li role="separator" class="divider"></li>
					<li>Code Region: '.$listeVilles["codeRegion"].'</li>
					<li role="separator" class="divider"></li>
					<li>'.$listeVilles["population"].' habitants</li>
					</ul>
					</div>';
			   $this->_idCity++;
		   }
		   
		   $this->setText($this->_city);
		 }
		 
		 private function getVilles(){
			preg_match('#((ville(s?))|(village(s?))) du ([0-9][0-9][0-9][0-9][0-9])#', $this->user_query, $this->_matches);
		   $this->_request=$this->_matches[6];
			 $this->getTableau("codePostal=".$this->_request);
			 $this->getVilleText();
		 }
		 
		 private function getVilleRegion(){
			preg_match('#((ville(s?))|(village(s?))) du ([0-9][0-9])#', $this->user_query, $this->_matches);
		   $this->_request=$this->_matches[6];
			 $this->getTableau("codeDepartement=".$this->_request);
			 $this->getVilleText();
		 }
		 
		 private function getCodePostal	(){
			 preg_match('#((code postal de(s?))|(ville(s?) de(s?))|(village(s?) de(s?))) (\w+)#', $this->user_query, $this->_matches);
			 $this->_request=$this->_matches[10];
			 $this->getTableau('nom='.$this->_request);
			 $this->getVilleText();
		 }
	}
