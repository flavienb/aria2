<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Learning extends Module {

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "learning"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }

    private function setLearn() {
        $array = getDataFromJson("../modules/learning/assets/data.json");
        $query = $this->user_query;
        if ($array !== false) {
            if (!isset($this->user_infos["question"])) {
                // QUESTION
                $exists = false;
                for ($i = 0; $i < sizeof($array); $i++) {
                    if ($array[$i]["question"] == $query) {
                        $exists = true;
                        break;
                    }
                }
                if ($exists === false) {
                    $n = sizeof($array);
                    $array[$n] = array("question" => $query, "answer" => "");
                    $this->setText($this->getModuleAnswer("answer_required"));
                } else {
                    $n = $i;
                    $this->setText($this->getModuleAnswer("already_put_wo_answer"));
                }
                $this->setInfos(array(
                    "function_name" => __FUNCTION__,
                    "data" => array(
                        "question" => $n
                    )
                ));
            } else {
                $explode = explode("|", trim($this->user_query));
                $query = $explode[0];
                $lang = (sizeof($explode) >= 2 && !empty($explode[1]) && in_array($explode[1], $this->languages)) ? $explode[1] : "en";
                $a = (isset($this->user_infos["question"])) ? intval($this->user_infos["question"]) : "no";
                if ($a !== "no" && isset($array[$a])) {
                    if (!isset($array[$a]["answer"][$lang])) {
                        // Langue à renseigner
                        if ($this->user_query == "eygh") {
                            $array[$a]["answer"][$lang] = "";
                        } else {
                            $array[$a]["answer"][$lang] = $query;
                        }
                        $this->setText($this->getModuleAnswer("answered_new_lang"));
                    } else {
                        // Langue déjà présente
                        if(isset($explode[2])) {
                            if ($this->user_query == "eygh") {
                                $array[$a]["answer"][$lang] = "";
                            } else {
                                $array[$a]["answer"][$lang] = $query;
                            }
                            $this->setText($this->getModuleAnswer("already_lang_overwrite"));
                        } else {
                            $array[$a]["answer"][$lang] .= $query;
                            $this->setText($this->getModuleAnswer("already_lang"));
                        }
                    }
                    $lang_tosay = (isset($this->languages_short_english[$lang])) ? $this->languages_short_english[$lang] : $lang;
                    $this->setText("The answer for : '" . $array[$a]["question"] . "' in " . $lang_tosay . " is '" . $query . "'.");
                } else {
                    $this->setText("Empty answer key, sorry.");
                }
            }
            $f = file_put_contents("../modules/learning/assets/data.json", json_encode($array));
            if ($f) {
                //$this->setText($this->getModuleAnswer("confirm"));
            } else {
                $this->setText("Sorry, I can't save this.");
            }
        } else {
            $this->setText("File does not exist.");
        }
    }

}
