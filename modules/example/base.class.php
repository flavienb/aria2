<?php

/*
 * Aria is an Open Source project.
 * Example module class.
 */

/**
 * Here, the module name is "Example". So the class name of this module needs to be the same : Example.
 * You have to add the name of your module in the array variable in modules/modules.php.
 * 
 * The class of your module is extended from the Module class for readability and simplicity reasons.
 * You can add any file used by your class in the same directory as your module. (For example, in this module add your files in modules/example/).
 * 
 */
/* -------------------------------------------------------------------------------------------- */
/**
 * Declared in the Module class (modules/module.class.php), the following variables and functions will be useful :
 * 
 * __[Functions]__
 * $this->setText($str); // Set the text to print AND tell. If called multiple times, text is added to the previous one.
 * $this->setImportantText($text); // Set the text that Aria will ONLY say but WON'T print. If empty, the text set with $this->setText(); will be told.
 * $this->getModuleAnswers[$key]; // $key is the key of an array in langs/en.json for example.
 * $this->getImportantText();
 * $this->setInfos($array("option1"=>"","option2"=>""[...])); // Used for interactions. Used to transmit data to the user browser to store ephemerally an information you can re-use for the next answer.
 * $this->setResourcesToLoad($array("../modules/my_module/js/resource1.css","../modules/my_module/js/resource2.js")); // Allows you to include CSS or JS files when returning the text to print to the user. You can specify a string. Please specify an absolute path.
 * $this->setPersistedInfos($array,$overwrite=false); // Persists data of the module in the profile. 
 * 
 * __[Variables]__
 * $this->user_query; (string) // Actual input sent by the user (what he wrote).
 * $this->user_infos; (array) // Contains the array previously set with setInfos(); and trasmitted by the user.
 * $this->user_data; (array) // Retrieve from the user profile the data persisted by the module; (automatically retrieve data stored in the ACTUAL MODULE)
 * $this->module_name; (string) // Returns the name of the actual module (here, Example).
 * $this->additional_infos; (array) // Return the array from the "others" key in langs/regex.xx.json.
 * 
 */

/*
 * FEEL FREE TO COPY PAST WHAT'S BELOW in the base.class.php file at the root of your module folder.
 * If you are a beginner, copy-past the entire module (example) folder, and rename it.
 */

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

/* DO NOT FORGET TO RENAME THIS MODULE ONCE YOU'VE COPY-PASTED IT (change 'class Example' to 'class MyClassName') */
class Example extends Module {
    
    /*
     * CRITICAL FUNCTIONS
     * CRITICAL FUNCTIONS ARE BELOW, DO NOT REMOVE.
     */
    /* Please DO NOT remove the __construct(); code to avoid errors. */

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "example"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }
    
    /* Please DO NOT remove the moduleCall(); code that is used to test if the module is correctly loaded. */
    public function moduleCall(){
        return true;
    }
    /*
     * END OF CRITICAL FUNCTIONS
     */

    /**
     * Then, add here all the functions you want !
     * For example, getText.
     * Prefer to use a getXxxx semantic and to declare private functions.
     */
    private function getText() {
        $this->setText("SUCCESS and additional infos are : "); // Set the text to show.
        $this->setText($this->additional_infos["first_additional_info"]); // Show what is inside the ["others"] array in langs/regex.xx.json.
    }

}
