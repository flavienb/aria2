<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Fiscentreprise extends Module {
    /* Please DO NOT remove the __construct(); code to avoid errors. */

	private $_codePostal, $_city, $_idCity, $_tableau, $_request, $_matches, $_curl;
	
    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "fiscentreprise"; // DO NOT FORGET TO MODIFY THE NAME OF YOUR MODULE
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }
	 
	 private function getSimulateur	(){
		 $this->setText('<div> <iframe id="SGMAPembauche" src="https://embauche.beta.gouv.fr/modules/v2/iframe.html?couleur=4A89DC" width="100%" style="border: none;
		 overflow: hidden; height: 548px;" scrolling="no"></iframe>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.5/iframeResizer.min.js"></script>
<script type="text/javascript">iFrameResize(null, \'#SGMAPembauche\')</script> </div>');
	}
}
