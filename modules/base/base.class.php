<?php

/*
 * Aria is an Open Source project.
 * Base module PHP class.
 */

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}

require(correctPathFichier("modules/module.class.php"));

class Base extends Module {

    protected $last_activity;

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "base";
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);

        /* Look for persisted data in user profile and update the $user_data variable. */
        $guifm = $this->user_data;
        if ($guifm !== false) {
            $this->last_activity = $guifm["last_activity"];
            $guifm["last_activity"] = time();
        } else {
            $guifm = array();
            $guifm["last_activity"] = time();
            $this->last_activity = $guifm["last_activity"];
        }
        /* Persists in profile and then actualizes the data */
        $this->setPersistedInfos($guifm);
        $this->user_data = $guifm;

        /* Manage the call of request function to call */
        /*
         * Checks if the user wants Aria to listen.
         */
        if (!isset($this->user_data["listen"]) || (isset($this->user_data["listen"]) && $this->user_data["listen"] == "yes")) {
            if ($function_to_call != "") {
                $this->$function_to_call();
            }
        } else if (( $this->user_data["listen"] == "no" && $function_to_call == "getStartListening")) {
            $this->$function_to_call();
        } else {
            $this->setText($this->getModuleAnswer("desactivated_listening"));
            $this->setImportantText(" ");
        }
    }

    /* Please DO NOT remove the moduleCall(); code that is used to test if the module is correctly loaded. */

    public function moduleCall() {
        return true;
    }

    private function getHmYes() {
        $this->setText($this->getModuleAnswer("hm_yes"));
    }

    private function getHmNo() {
        $this->setText($this->getModuleAnswer("hm_no"));
    }

    private function switchLangTo() {
        $preg = preg_match("#" . $this->additional_infos["pattern"] . "#", $this->user_query, $matches);
        $size = sizeof($matches) - 1;
        $matches = trim($matches[$size]);
        if ($size > 0 && isset($matches) && $preg && (in_array($matches, $this->languages) || isset($this->languages_full[$matches]))) {
            if (in_array($matches, $this->languages)) {
                $l = $matches;
            } elseif (isset($this->languages_full[$matches])) {
                $l = $this->languages_full[$matches];
            } else {
                $l = false;
            }
            $this->setDebug($l);
            $path = "../modules/base/js/changeLang_" . $l . ".js";
            if (file_exists($path) && $l !== false) {
                $this->setResourcesToLoad($path);
                $this->setText($this->getModuleAnswer("successfuly_sent_file"));
            } else {
                $this->setText($this->getModuleAnswer("inexistant_file"));
            }
        } else {
            $this->setText($this->getModuleAnswer("wrong_lang"));
        }
    }

    private function getTime() {
        $time_zone = getTimeZone(getIp());
        date_default_timezone_set($time_zone);
        $time = time();
        $to_say = $this->getModuleAnswer("hour") . date("G:i A", $time);
        if ($time_zone == "Europe/Paris") {
            $this->setText($this->getModuleAnswer("hour") . ' <table width="150" border="0" cellpadding="0" cellspacing="0" STYLE="border-color: #cccccc ; border-width: 1px;border-style: solid;"> <!--DWLayoutTable--> <tr> <td width="150" height="19"><div align="center"> <IFRAME src="http://www.Heure.com/heure-fr.php?timezone=1&size=12" WIDTH="50px" HEIGHT="15px" BORDER="0" MARGINWIDTH="0" MARGINHEIGHT="10" HSPACE="0" VSPACE="0" FRAMEBORDER="0" SCROLLING="no"> </IFRAME> </div></td> </tr> <tr> <td height="12" valign="top"><div align="center"><font face="Arial, Helvetica, sans-serif" size="1">Powered by <a href="http://www.Heure.com" style="color:#000000;text-decoration:none; ">Heure.com</a></font></div></td> </tr> </table>');
        } else {
            $this->setText($to_say);
        }
        $this->setImportantText($to_say);
    }

    private function getDate() {
        date_default_timezone_set("Europe/Paris"); // To modify according to the localisation.
        $time = time();
        if ($this->language == "fr") {
            $this->setText($this->getModuleAnswer("date") . strftime("%A %d %B %Y"));
        } else {
            $this->setText($this->getModuleAnswer("date") . date("l jS \of F Y h:i:s A", $time));
        }
    }

    private function howAreYou() {
        if ($this->user_infos == "") {
            if ($this->language == "en") {
                $answer = array(
                    "Thank you " . $_SESSION['user']['first_name'] . ", what about you ?"
                );
            } else if ($this->language == "fr") {
                $answer = array(
                    "Merci " . $_SESSION['user']['first_name'] . ", et vous ?"
                );
            }
            $answer = array_merge($answer, $this->module_answers["howareyou_to_answer"]);
            $this->setText($answer[rand(0, sizeof($answer) - 1)]);
            $this->setInfos(array(
                "function_name" => __FUNCTION__,
                "data" => array(
                    "first_question" => $this->user_query
                )
            ));
        } else {
            if (!isset($this->user_infos["bad_day"])) {
                if (preg_match("#" . $this->additional_infos["howareyou_good_day_pattern"] . "#i", $this->user_query)) {
                    $this->setText($this->getModuleAnswer("howareyou_good_day"));
                } else {
                    $r = rand(0, 1);
                    if ($r == 1) {
                        // With answer required from user
                        $answer = $this->setText($this->getModuleAnswer("howareyou_bad_day_to_answer"));
                        $this->setInfos(array(
                            "function_name" => __FUNCTION__,
                            "data" => array(
                                "bad_day" => $answer[$r]
                            )
                        ));
                    } else {
                        // With no answer required
                        $this->setText($this->getModuleAnswer("howareyou_bad_day_answered"));
                    }
                }
            } else {
                // Answers from "bad_day" interaction.
                $this->setText($this->getModuleAnswer("howareyou_bad_day"));
            }
        }
    }

    private function getQuote() {
        if (!isset($this->user_infos["quotes"])) {
            if ($this->language == "en") {
                $quote = $this->pickQuote();
                $this->setText($quote);
                $this->setInfos(array("function_name" => __FUNCTION__,
                    "data" => array(
                        "quotes" => ""
                    )
                ));
                $this->setText($this->module_answers["quotes"]["another_quote_bis"]);
            } else {
                $q = $this->getModuleAnswer("not_available");
                $this->setText($q);
            }
        } else {
            if (preg_match("#" . $this->additional_infos["quote_answers_pattern"] . "#", $this->user_query)) {
                unset($this->user_infos["quotes"]);
                $this->getQuote();
            } else {
                $this->setText("OK! =)");
            }
        }
    }

    private function pickQuote() {
        include(correctPathFichier("assets/api/api_external_keys.php"));
        $res = curlPost("https://andruxnet-random-famous-quotes.p.mashape.com/?cat=famous", array(), array(
            "X-Mashape-Key: $apiKey_mashape",
            "Content-Type: application/x-www-form-urlencoded",
            "Accept: application/json"
                )
        );
        if (intval($res["status"]) == 200) {
            $response = json_decode($res["response"], true);
            $r = $response["quote"] . "<br/> (From <i>" . $response["author"] . "</i>)";
            $this->setImportantText($response["quote"] . $this->getModuleAnswer("quotes", "another_quote"));
            return $r;
        } else {
            $quotes = json_decode(file_get_contents("../modules/base/quotes.json"), true);
            $r = rand(0, sizeof($quotes));
            $this->setImportantText($quotes[$r]["quote"] . $this->getModuleAnswer("quotes", "another_quote"));
            return $quotes[$r]["quote"] . "<br/> (From <i>" . $quotes[$r]["name"] . "</i>)";
        }
    }

    private function getYey() {
        $sentences = $this->setText($this->getModuleAnswer("yey"));
    }

    private function getNothingToSay() {
        $sentences = $this->setText($this->getModuleAnswer("nothing_to_say"));
    }

    private function getObviousness() {
        $sentences = $this->setText($this->getModuleAnswer("obviousness"));
    }

    private function getHelloAria() {
        $sentences = array(
            "Hey" . (rand(0, 100) % 2 == 0) ? ' ' . $_SESSION['user']['first_name'] : '' . ", what can I do for you ?",
            "Hi" . (rand(0, 100) % 2 == 0) ? ' ' . $_SESSION['user']['first_name'] : '' . ", what can I do for you ?",
            "Yes ?",
            "It's me !",
            "Yes " . (rand(0, 100) % 2 == 0) ? $_SESSION['user']['first_name'] : '' . " ?"
        );
        $r = rand(0, sizeof($sentences) - 1);
        $this->setText($sentences[$r]);
    }

    private function getHello() {
        /* First Aria function ! =) */
        $funny = array(
            "<audio controls autoplay>
  <source src='../assets/sounds/hello_its_me.mp3' type='audio/mpeg''>
Your browser does not support the audio element.
</audio>"
        );
        $sentences_day = $this->getModuleAnswer("hello_sentences_day");
        $sentences_night = $this->getModuleAnswer("hello_sentences_night");
        $sentences_back = $this->getModuleAnswer("hello_sentences_back");
        $sentences_justbefore = $this->getModuleAnswer("hello_sentences_justbefore");
        $no_last_activity = true;

        if (isset($this->user_data["last_activity"]) && $this->user_data["last_activity"] != "") {
            if ((time() - intval($this->last_activity)) > 12 * 3600) {
                /* If it makes +12h that user haven't said "Hello" to Aria */
                $no_last_activity = false;
                $h = $sentences_back;
            } else if (time() - intval($this->last_activity) < 3600) {
                /* If it does -1 hour that user haven't said "Hello" to Aria 
                 * (kinda strange that someone tells you hello whereas you told him hello a bit ago no ?) 
                 */
                $no_last_activity = false;
                $h = $sentences_justbefore;
            }
        }

        if (rand(0, 20) == 4) {
            $this->setText($funny[rand(0, sizeof($funny) - 1)]);
            $this->setImportantText(" ");
        } elseif ($no_last_activity) {
            /* If it seems like a legit hello */
            if (is_between(date("H", time()), 5, 12)) {
                /* Morning */
                $h = $sentences_day;
            } else {
                /* Evening */
                $h = $sentences_night;
            }
        }
        $this->setText($h);
    }

    private function getHowsYourDay() {
        if (!isset($this->user_infos["howsyourday"])) {
            $this->setText($this->getModuleAnswer("hows_your_day", "first_ask"));
            $this->setInfos(array(
                "function_name" => __FUNCTION__,
                "data" => array(
                    "howsyourday" => ""
                )
            ));
        } else {
            if (preg_match("#" . $this->additional_infos["howareyou_good_day_pattern"] . "#i", $this->user_query)) {
                $this->setText($this->getModuleAnswer("howareyou_good_day"));
            } else {
                $this->setText($this->getModuleAnswer("howareyou_bad_day"));
            }
        }
    }

    private function getDefinition() {
        include(correctPathFichier("assets/api/api_external_keys.php"));
        if (in_array($this->language, array("en", "fr"))) {
            $r = preg_match("#" . $this->additional_infos["pattern"] . "#", $this->user_query, $matches);
            switch ($this->language) {
                case "en":
                    $word = $matches[9];
                    $res = curlGet("https://wordsapiv1.p.mashape.com/words/" . $word, $array_mashape);
                    $response = json_decode($res["response"], true);
                    if (is_array($response)) {
                        if (isset($response["results"])) {
                            $i = 0;
                            foreach ($response["results"] as $definition) {
                                $this->setText("$word : " . $response["results"][$i]["definition"] . ".<br/><br/>");
                                $i++;
                            }
                            if (sizeof($response["results"]) > 1) {
                                $this->setImportantText($this->getModuleAnswer("definition_severalfound"));
                            }
                        } else {
                            $this->setText($word . " ? " . $this->getModuleAnswer("definition_notfound"));
                        }
                    } else {
                        $this->setText($this->getModuleAnswer("definition_error"));
                        $this->setInfos(array(
                            "returned" => $res,
                            "debug_curl" => $res["debug"]
                        ));
                    }
                    break;
                case "fr":
                    $word = $matches[7];
                    $def = getHtmlElement("http://www.larousse.fr/dictionnaires/francais/" . $word, ".Definitions");
                    if (empty($def)) {

                        $this->setText($word . " ? " . $this->getModuleAnswer("definition_notfound"));
                    } else {
                        /* Here, the ouput needs many treatments because the definition is extracted from a website (HTML), not an API */
                        $def = html_entity_decode(utf8_decode(strip_tags($def)));
                        $expl = explode(".", $def);
                        $def = $expl[0] . ".";
                        $example_cut = explode(":", $def);
                        if (sizeof($example_cut) > 1) {
                            $this->setText($example_cut[0] . ". Par exemple : \"" . $example_cut[1] . "\".");
                        } else {
                            $this->setText($def);
                        }
                    }
                    break;
                default:
                    $this->setText($this->getModuleAnswer("not_available"));
            }
        } else {
            $this->setText($this->getModuleAnswer("not_available"));
        }
    }

    /*
      private function getWhois() {
      include(correctPathFichier("assets/api/api_external_keys.php"));
      if (in_array($this->language, array("en", "fr"))) {
      $r = preg_match("#" . $this->additional_infos['pattern'] . "#", $this->user_query, $matches);
      $word = $matches[7];
      switch ($this->language) {
      case "en":
      $res = curlGet("https://mutationevent-whois.p.mashape.com/", $array_mashape);
      }
      }
      }
     * 
     */

    private function getIpInfos() {
        include(correctPathFichier("assets/api/api_external_keys.php"));
        $r = preg_match("#" . $this->additional_infos["pattern"] . "#", $this->user_query, $matches);
        $word = str_replace(' ', '', $matches[0]);
        $res = curlGet("https://ipinfo.p.mashape.com/" . $word . "/json", $array_mashape);
        $res = json_decode($res["response"], true);
        $this->setText(build_table($res));
        $this->setImportantText($this->getModuleAnswer("found"));
    }

    private function getSynonym() {
        include(correctPathFichier("assets/api/api_external_keys.php"));
        if (in_array($this->language, array("en", "fr"))) {
            $r = preg_match("#" . $this->additional_infos["pattern"] . "#", $this->user_query, $matches);
            $word = $matches[7];
            switch ($this->language) {
                case "en":
                    $res = curlGet("https://wordsapiv1.p.mashape.com/words/" . $word . "/similarTo", $array_mashape);
                    $response = json_decode($res["response"], true);
                    if (is_array($response)) {
                        if (isset($response["similarTo"]) && !empty($response["similarTo"])) {
                            $arr_syns = $response["similarTo"];
                            $syns = "";
                            $size = (isset($matches[1]) && $matches[1] != "") ? $matches[1] : sizeof($arr_syns);
                            for ($i = 0; $i < $size; $i++) {
                                if ($size - 1 == $i) {
                                    $syns .= $arr_syns[$i];
                                } else {
                                    $syns .= $arr_syns[$i] . "<span class='red-dot'>,</span> ";
                                }
                            }
                            $this->setText("$word : " . $syns . ".");
                        } else {
                            $this->setText($word . " ? " . $this->getModuleAnswer("synonym_notfound"));
                        }
                    } else {
                        $this->setText($this->getModuleAnswer("synonym_error"));
                        $this->setInfos(array(
                            "returned" => $res,
                            "debug_curl" => $res["debug"]
                        ));
                    }
                    break;
                case "fr":
                    $res = curlPost("https://api.berwick.fr/apis/frsynonyms/", array(
                        "api_key" => $apiKey_berwick,
                        "word" => $matches[8]
                    ));
                    $res = json_decode($res["response"], true);
                    $return = "";
                    if (!empty($res) && $res["success"] == "true") {
                        $ex = explode(",", $res["synonyms"]);
                        if (isset($matches[1]) && !empty($matches[1])) {
                            $wtn = new WordsToNumbers($this->language);
                            $max = $wtn->convert($matches[1]);
                        } else {
                            $max = sizeof($ex);
                        }
                        for ($i = 0; $i < $max; $i++) {
                            if ($i + 1 == $max) {
                                $return .= utf8_decode($ex[$i]);
                            } else {
                                $return .= utf8_decode($ex[$i]) . ", ";
                            }
                        }
                        $this->setText($return);
                    } else {
                        $this->setText($this->getModuleAnswer("synonym_notfound"));
                    }
                    break;
                default:
                    $this->setText($this->getModuleAnswer("not_available"));
            }
        } else {
            $this->setText($this->getModuleAnswer("not_available"));
        }
    }

    private function getJoke() {
        $joke = curlGet("http://zloutek-soft.cz/json-jokes/");
        if ($joke["status"] == "200") {
            $this->setText($joke["response"]);
        } else {
            $jokes = getDataFromJson(correctPathFichier("modules/base/assets/jokes.json"));
            $this->setText($jokes["value"][rand(0, sizeof($jokes["value"]) - 1)]["joke"]);
        }
    }

    private function getStopListening() {
        $this->setText($this->getModuleAnswer("stop_listening"));
        $this->user_data["listen"] = "no";
        $this->setPersistedInfos($this->user_data);
    }

    private function getStartListening() {
        $this->setText($this->getModuleAnswer("start_listening"));
        $this->user_data["listen"] = "yes";
        $this->setPersistedInfos($this->user_data);
    }

    private function getCalories() {
        if (in_array($this->language, array("en"))) {
            $calories = json_decode(file_get_contents("../modules/base/assets/calories_en.json"), true);
            $total_calories = 0;
            $found_one_ingredient = false;
            $items_unknown = array(); // All unknown aliments are here.
            $sum = 0;
            $items = array();
            $preg = preg_match("#" . $this->additional_infos["pattern"] . "#", $this->user_query, $patterns);
            $int_word_1 = 11;

            if (isset($patterns[$int_word_1])) {
                if (isset($calories[$patterns[$int_word_1]])) {
                    $found_one_ingredient = true;
                    array_push($items, array("name" => $patterns[$int_word_1]));
                    if (isset($patterns[9]) && is_numeric($patterns[9])) {
                        $items[sizeof($items) - 1]["grams"] = intval($patterns[9]);
                    } else if (isset($patterns[5]) && strlen($patterns[5]) != 0) {
                        if (isset($calories[$patterns[$int_word_1]]["portion"])) {
                            $items[sizeof($items) - 1]["grams"] = $calories[$patterns[$int_word_1]]["portion"];
                        } else {
                            $items[sizeof($items) - 1]["grams"] = 100;
                        }
                    } else {
                        $items[sizeof($items) - 1]["grams"] = 100;
                    }
                    $actual_cals = ($items[sizeof($items) - 1]["grams"] * $calories[$patterns[$int_word_1]]["calories"]) / 100;
                    $items[sizeof($items) - 1]["calories"] = $actual_cals;
                    $total_calories += $actual_cals;
                } else {
                    array_push($items_unknown, array("name" => $patterns[$int_word_1]));
                }
            } else {
                
            }

            $int_word_2 = 20;
            if (isset($patterns[$int_word_2])) {
                if (isset($calories[$patterns[$int_word_2]])) {
                    $found_one_ingredient = true;
                    array_push($items, array("name" => $patterns[$int_word_2]));
                    if ((!isset($patterns[17]) || strlen($patterns[17]) == 0) && isset($patterns[18]) && is_numeric($patterns[18])) {
                        // xx grams...
                        $items[sizeof($items) - 1]["grams"] = intval($patterns[18]);
                    } else if (isset($patterns[17])) {
                        // a portion of...
                        if (isset($calories[$patterns[$int_word_2]]["portion"])) {
                            $items[sizeof($items) - 1]["grams"] = $calories[$patterns[$int_word_2]]["portion"];
                        } else {
                            $items[sizeof($items) - 1]["grams"] = 100;
                        }
                    } else {
                        $items[sizeof($items) - 1]["grams"] = 100;
                    }
                    $actual_cals = ($items[sizeof($items) - 1]["grams"] * $calories[$patterns[$int_word_2]]["calories"]) / 100;
                    $items[sizeof($items) - 1]["calories"] = $actual_cals;
                    $total_calories += $actual_cals;
                } else {
                    array_push($items_unknown, array("name" => $patterns[$int_word_2]));
                }
            } else {
                
            }

            $answer = "<table class='table'>"
                    . "<tr>"
                    . "<th>" . $this->getModuleAnswer("name") . "</th>"
                    . "<th>" . $this->getModuleAnswer("calories_ingredient_asked") . "</th>"
                    . "<th>" . $this->getModuleAnswer("calories_ingredient_100g") . "</th>"
                    . "</tr>";
            for ($i = 0; $i < sizeof($items); $i++) {
                $answer .= "<tr>"
                        . "<td>" . $items[$i]['name'] . "</td>"
                        . "<td>" . $items[$i]['calories'] . " calories in " . $items[$i]['grams'] . "g</td>"
                        . "<td>" . $calories[$items[$i]['name']]['calories'] . "</td>"
                        . "</tr>";
                if ($i == 0) {
                    $this->setImportantText("There's " . $items[$i]['calories'] . " calories in " . $items[$i]['grams'] . " grams of " . $items[$i]['name'] . ", ");
                } else {
                    $this->setImportantText("and " . $items[$i]['calories'] . " calories in " . $items[$i]['grams'] . " grams of " . $items[$i]['name'] . ".", true);
                }
            }

            $answer .= "</table><br/><br/>"
                    . "<table class='table'><tr>"
                    . "<th>" . $this->getModuleAnswer("name") . " (" . $this->getModuleAnswer("not_found") . ")</th>"
                    . "</tr>";

            for ($i = 0; $i < sizeof($items_unknown); $i++) {
                $answer .= "<tr><td>" . $items_unknown[$i]['name'] . "</td></tr>";
                if ($i == 0) {
                    $this->setImportantText($this->getModuleAnswer("calories_ingredient_not_found") . $items_unknown[$i]['name'], true);
                } else {
                    $this->setImportantText(", and " . $items[$i]['name'] . ".", true);
                }
            }
            $answer .= "</table>";

            $this->setText($answer);
        } else {
            $this->setText($this->getModuleAnswer("not_available"));
        }
    }

    private function getCalculus() {
        include("assets/Field_Calculate.php");
        $Cal = new Field_calculate();
        $replace_from = $this->additional_infos["replace_from"];
        $replace_to = $this->additional_infos["replace_to"];
        $qu = $this->user_query;
        $qu = preg_replace($replace_from, $replace_to, $qu);
        $result = @$Cal->calculate($qu);
        $result = $this->toComa($result);
        $this->setText($result);
    }

    private function getColor() {
        //$this->setResourcesToLoad(array());
        preg_match("#" . $this->additional_infos["pattern"] . "#", $this->user_query, $patterns);
        if (preg_match("#[0-9a-fA-F]{6}#", $patterns[2])) {
            $color = "#" . $patterns[2];
        } else {
            $color = $patterns[2];
        }
        $this->setText("<style>body{background-color:" . $color . ";}</style>");
        $this->setText("This is " . $this->user_query);
        $this->setImportantText("Here's the color " . $color . ".");
    }

    private function getModulesList() {
        include("../modules/modules.php");
        // $enabled_modules
        $modules = "none";
        /*
         * Getting activated module names.
         */
        for ($i = 0; sizeof($enabled_modules) > $i; $i++) {
            if ($i == 0) {
                $modules = $enabled_modules[$i];
            } else {
                $modules .= ", " . $enabled_modules[$i];
            }
            if ($i + 1 == sizeof($enabled_modules)) {
                $modules .= ".";
            }
        }
        $this->setText("Modules loaded : " . $modules);
        /*
         * Checking existence of critical files.
         */
        $this->setText("<br/>" . "Checking the files of the modules.");
        $all_modules_loaded = true;
        foreach ($enabled_modules as $module) {
            $loaded = false;
            $actual_string = "";
            $actual_string .= "Module <b>" . $module . "</b> : ";
            if (folder_exist("../modules/" . $module)) {
                $actual_string .= "folder exists, ";
                if (file_exists("../modules/" . $module . "/base.class.php")) {
                    $actual_string .= "module class exists, ";
                    if(file_exists("../modules/".$module."/langs/regex.en.json")) {
                        $actual_string .= "english regex exists, ";
                        $loaded = true;
                    } else {
                        $actual_string .= "english regex doesn't exist.";
                    }
                } else {
                    $actual_string .= "module class doesn't exist.";
                }
            } else {
                $actual_string .= "folder does not exist.";
            }
            if ($loaded) {
                $actual_string .= "<span style='color:green;'>correctly loaded</span>.";
            } else {
                $all_modules_loaded = false;
                $actual_string .= "<span style='color:red;'>not loaded</span>.";
            }
            $this->setText("<br/>" . $actual_string);
        }
        if ($all_modules_loaded) {
            $this->setImportantText("Everything seems OK.");
        } else {
            $this->setImportantText("At least one module isn't correctly loaded, surely a missing file.");
        }
    }

    private function getTest() {
        $this->setText("Working.");
    }

    private function answerThanks() {
        $array = $this->setText($this->getModuleAnswer("thanks"));
    }

    /* ===========DO NOT USE OR REFER TO THE FOLLOWINGS========= 
     * Those are system functions not respecting the modele_reponse.json.
     * Please do not use theses as example.
     * Never call directly methods of a module.
     */

    public function getUnknownAnswer() {
        if (!isset($this->user_data["listen"]) || $this->user_data["listen"] == "yes") {
            if (isset($this->module_answers["unknown_answers"])) {
                $sentence = $this->getModuleAnswer("unknown_answers");
            } else {
                $sentences = array(
                    "I haven't found the language asked.",
                    "This language does not exist yet.",
                    "Well, this language is not ready yet.",
                    "Please select an available language... Like English!"
                );
                $sentence = $sentences[rand(0, sizeof($sentences) - 1)];
            }
            return $sentence;
        } else {
            return array($this->getModuleAnswer("desactivated_listening"), " ");
        }
    }

}

?>