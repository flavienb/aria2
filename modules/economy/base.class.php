<?php

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Economy extends Module {
    /* Please DO NOT remove the __construct(); code to avoid errors. */

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "economy";
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }

    /* Please DO NOT remove the moduleCall(); code that is used to test if the module is correctly loaded. */

    public function moduleCall() {
        return true;
    }

    private function getConvertCurrency() {
        include(correctPathFichier("assets/api/api_external_keys.php"));
        $replace_from = $this->additional_infos["replace_from"];
        $replace_to = $this->additional_infos["replace_to"];
        $qu = preg_replace($replace_from, $replace_to, $this->user_query);
        $q = preg_match("#" . $this->additional_infos["pattern"] . "#", $qu, $matches);
        $n = $this->convertToNumber($matches[2]);
        $c1 = (isset($matches[1]) && $matches[1] != "") ? $matches[1] : $matches[6];
        $c2 = $matches[10];
        $res = curlGet("http://api.fixer.io/latest?base=" . $c1);
        $res = json_decode($res["response"], true);
        if (isset($res["rates"]) && $res["base"] == $c2) {
            $this->setText($this->getModuleAnswer("currency_same") . " " . $n . ".");
        } else if (isset($res["rates"])) {
            $sum = round($n * floatval($res["rates"][$c2]), 3);
            if ($this->language == "fr") {
                $sum = $this->toComa($sum);
            }
            $this->setText($n . " " . $c1 . " " . $this->getModuleAnswer("currency_are") . " " . $sum . " " . $c2 . ".");
            $this->setImportantText($sum);
        } else {
            var_dump($res);
            $this->setText($this->getModuleAnswer("currency_error"));
        }
    }

}
