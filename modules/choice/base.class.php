<?php

/*
 * Aria is an Open Source project.
 * Example module class.
 */

if (file_exists("../../assets/functions.php")) {
    require("../../assets/functions.php");
}
require(correctPathFichier("modules/module.class.php"));

class Choice extends Module {

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        $this->module_name = "choice";
        $args = func_get_args();
        call_user_func_array(array($this, 'parent::__construct'), $args);
        /* Manage the call of request function to call */
        if ($function_to_call != "") {
            $this->$function_to_call();
        }
    }

    public function getResponse() {
        $answer=$this->user_query;
        $array=explode("ou",$answer);
        $r=rand(0,sizeof($array)-1);
        $this->setText($array[$r]);
    }

}
