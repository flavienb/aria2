<?php

/*
 * Aria is an Open Source project.
 * Module class, master class that all modules class inherit of. 
 */

class Module {

    public $module_name = "";
    protected $answer = array(
        "text" => "",
        "text_important" => "",
        "infos" => "{'request_answer':'no'}",
        "resources_to_load" => ""
    );
    protected $user_data = ""; // Persisted user data relating to the module.
    protected $user_infos = ""; // Data sent during the previous question. (Interaction).
    protected $additional_infos = "";
    protected $user_query = "";
    protected $module_answers = "";
    protected $language;
    protected $languages = array(
        "en",
        "fr"
    );
    protected $languages_full = array(
        // English
        "english" => "en",
        "anglais" => "en",
        // French
        "french" => "fr",
        "français" => "fr"
    );
    protected $languages_short_english = array(
        "en"=>"english",
        "fr"=>"french",
    );

    function __construct($language = "en", $query, $others = "", $function_to_call = "", $infos = "") {
        /* Stores user infos transmitted by the previous answer */
        $this->user_query = html_entity_decode($query);
        $this->additional_infos = (isset($others)) ? $others : "";
        if ($infos != "") {
            $this->user_infos = (isset($infos["data"])) ? $infos["data"] : "";
        }
        $this->getPersistedInfos();
        $this->language = $language;
        $this->loadAnswers();
    }

    protected function loadAnswers() {
        $languages = $this->languages;
        $sc = scandir("../modules/" . $this->module_name . "/langs");
        $valid_lang = false;
        for ($i = 2; $i < sizeof($sc); $i++) {
            $act = explode(".", $sc[$i]);
            if (in_array($act[0], $languages) && $this->language == $act[0]) {
                $valid_lang = $act[0];
                break;
            }
        }
        if ($valid_lang !== false) {
            $file_path = "../modules/" . $this->module_name . "/langs/" . $valid_lang . ".json";
            $this->module_answers = getDataFromJson($file_path);
        } else {
            $this->module_answers = "Language not found.";
            $this->setText("Language not found.");
        }
    }

    protected function setInfos($array) {
        $mn = array("request_answer" => "yes", "module_name" => $this->module_name);
        $data = array_merge($mn, $array);
        $this->answer["infos"] = json_encode($data);
    }

    protected function getPersistedInfos() {
        $guifm = getUserInfosForModule($this->module_name, $_SESSION["user"]["username"]);
        $this->user_data = $guifm;
    }

    protected function setPersistedInfos($array, $overwrite = false) {
        if ($overwrite) {
            return setUserInfosForModule($this->module_name, $_SESSION["user"]["username"], $array);
        } else {
            if(isset($this->user_data)&&is_array($this->user_data)){
                $arr=$this->user_data;
            }else{
                $arr=array();
            }
            $ar = array_merge($array, $arr);
            return setUserInfosForModule($this->module_name, $_SESSION["user"]["username"], $array);
        }
    }

    protected function setText($text, $add = true) {
        if ($add) {
            if ($this->answer["text"] == "") {
                $this->answer["text"] .= $text;
            } else {
                $this->answer["text"] .= "\n\n" . $text;
            }
        } else {
            $this->answer["text"] = $text;
        }
        return $text;
    }

    protected function setImportantText($text, $add = true) {
        if ($add) {
            if ($this->answer["text_important"] == "") {
                $this->answer["text_important"] .= $text;
            } else {
                $this->answer["text_important"] .= " " . $text;
            }
        } else {
            $this->answer["text_important"] = $text;
        }
    }

    protected function getText() {
        return $this->answer["text"];
    }

    protected function getImportantText() {
        return $this->answer["text_important"];
    }

    protected function setResourcesToLoad($array) {
        if (is_array($array)) {
            $data = array_merge($this->answer, $array);
        } else {
            if (is_array(json_decode($this->answer["resources_to_load"]))) {
                $data = json_decode($this->answer["resources_to_load"]);
                array_push($this->answer["resources_to_load"], $data);
            } else {
                $data = array($array);
            }
        }
        $this->answer["resources_to_load"] = json_encode($data);
    }

    protected function setDebug($text) {
        $this->answer["debug"] = $text;
    }

    protected function getModuleAnswer($key, $second_key = false, $strict = false) {
        /*
         * Automatically choose a random answer
         */
        if ($second_key === false) {
            $answers = $this->module_answers[$key];
        } else {
            $answers = $this->module_answers[$key][$second_key];
        }
        if ($strict === false) {
            $r = rand(0, sizeof($answers) - 1);
            return $answers[$r];
        } else {
            return $answers;
        }
    }

    public function getAnswer() {
        if ($this->user_infos != "") {
            $this->user_infos = json_encode($this->user_infos);
        }
        return $this->answer;
    }

    /*
     * Other functions used by the modules
     */

    protected function convertToNumber($str) { // Check if the dot or the coma is correct to convert a string to an int.
        return floatval(preg_replace("/,/", ".", $str));
    }

    protected function toComa($str) {
        return preg_replace("/[.]/", ",", $str);
    }

}
