<?php
require("assets/header.php");
if (isset($_SESSION["user"])) {
    header("Location:me/");
}
$token = md5(time());
$_SESSION["token_login"] = md5($token . getIp());
?>
<div class="jumbotron vertical-center">
    <div class="container">
        <div class="col-lg-6">
            <form method="POST" action="" id="login">
                <div class="form-group">
                    <label for="user">Your username :</label>
                    <input type="text" name="user" class="form-control" aria-describedby="userHelp" pattern="^[a-zA-Z]{1,30}\d{0,4}$"/>
                    <small id="userHelp" class="form-text text-muted">Enter your username.</small>
                </div>
                <div class="form-group">
                    <label for="password">Your password :</label>
                    <input type="password" name="password" class="form-control" aria-describedby="passwordHelp"/>
                    <small id="passwordHelp" class="form-text text-muted">Enter your password.</small>
                </div>
                <div class="form-group">
                    <button type="submit" name="sbm" class="btn btn-primary">Authentification</button>
                </div>
                <input type="hidden" name="token_login" value="<?php echo $token; ?>"/>
            </form>
            <a href="register.php" style="float:right;">Not registered yet ?</a>
        </div>
    </div>
</div>
<?php
$more = "<script src='assets/js/login.js'></script>";
require("assets/footer.php");
?>