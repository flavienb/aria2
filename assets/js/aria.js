/* 
 * Aria is an Open Source project.
 * Aria JS Used in me/index.php.
 */

/* 
 * Aria is an Open Source project.
 */
$(document).ready(function () {
    var circles = $(".circle");
    var sbm = $('[name="sbm"]');
    var tinput = $('[name="text"]');

    $('#aria').on('submit', function (e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire
        work();
    });

    artyom.addCommands([
        {
            indexes: ['*'],
            smart: true,
            action: (i, wildcard) => {
                tinput.val(wildcard);
                work();
            }
        }
    ]);

// Start the commands !
    artyom.initialize({
        lang: "en-GB", // GreatBritain english
        continuous: true, // Listen forever
        soundex: true, // Use the soundex algorithm to increase accuracy
        debug: true, // Show messages in the console
        listen: true // Start to listen commands !
    }).then(() => {
        console.log("Atryom started.");
    }).catch((err) => {
        console.error("Artyom couldn't be initialized: ", err);
    });

    function startAriasMindAnimation() {
        sbm.attr('disabled', 'disabled');
        sbm.val("Looking for your question.");
        circles.fadeOut(500);
        $('.plane.main').css({
            "-webkit-animation": "rotate 3s infinite linear",
            "animation": "rotate 3s infinite linear"
        });
        circles.fadeIn(500);
    }

    function stopAriasMindAnimation() {
        circles.fadeOut(200);
        $('.plane.main').css({
            "-webkit-animation": "rotate 20s infinite linear",
            "animation": "rotate 20s infinite linear"
        });
        circles.fadeIn(200);
        sbm.removeAttr('disabled');
        sbm.val("Send");
    }

    function work() {
        console.log("Work started.");
        var $this = $('#aria');
        var text = $('[name="text"]');
        var answer_tb = $('#answer_tb');
        var circles = $(".circle");
        var infos = $("[name='infos']");
        var token = $("[name='token']");
        var additional_resources = $("#additional_resources");
        var lang = $('input[name=lang]:checked').val();
        var lang_speech = "en";
        if (text.val() === '') {
            $.notify("Hmm. It seems like you're doing the sound of silence.", "warn");
        } else {
            additional_resources.html("");
            answer_tb.html("");
            startAriasMindAnimation();
            $.ajax({
                url: "processing.php",
                type: $this.attr('method'),
                data: $this.serialize(),
                dataType: 'json',
                success: function (json) {
                    if (json.is_allowed_to_ask === "yes") {
                        if (json.text) {
                            if (lang == 'fr') {
                                lang_speech = 'fr-FR';
                            }
                            text.val("");
                            var msg = new SpeechSynthesisUtterance();
                            var voices = window.speechSynthesis.getVoices();
                            msg.volume = 1; // 0 to 1
                            msg.rate = 1; // 0.1 to 10
                            msg.pitch = 1; //0 to 2
                            msg.lang = lang_speech;
                            if (json.text_important && json.text_important.length > 0) {
                                msg.text = json.text_important;
                                speechSynthesis.speak(msg);
                            } else if (json.text.length <= 100) {
                                msg.text = json.text;
                                speechSynthesis.speak(msg);
                            }
                            // speechSynthesis.cancel()

                            answer_tb.html(json.text);
                            /* If additional JS or CSS resources */
                            if (json.resources_to_load) {
                                resources=$.parseJSON(json.resources_to_load);
                                $.each(resources, function (i, val) {
                                    var valsub = val.substr(-3);
                                    if (valsub === ".js") {
                                        additional_resources.append("<script src='" + val + "'></script>");
                                    } else if (valsub === "css") {
                                        additional_resources.append("<link rel='stylesheet' href='" + val + "'/>");
                                    }
                                });
                            }
                            if (json.infos) {
                                infos.val(json.infos);
                            }
                        } else {
                            $.notify("It seems like I don't have the answer to this question, sorry.", "error");
                        }
                    } else {
                        $.notify("You are not allowed to ask any. Please wait some minutes.", "error");
                    }
                    if (json.new_token) {
                        token.val(json.new_token);
                    } else {
                        $.notify("Wow, this module did not gave me a token. Please reload the page.", "warn");
                    }
                    stopAriasMindAnimation();
                },
                error: function () {
                    $.notify("It seems like I can't connect, please check your internet connection.", "error");
                    circles.fadeIn(500);
                    circles.css({
                        "box-shadow": "0 0 60px red, inset 0 0 60px red"
                    });
                    var delay = 2000;
                    setTimeout(function () {
                        circles.fadeOut(500);
                        circles.css({
                            "box-shadow": "0 0 60px #5e0d80, inset 0 0 60px #5e0d80"
                        });
                        circles.fadeIn(500);
                    }, delay);
                    stopAriasMindAnimation();
                }
            });
        }
    }
});