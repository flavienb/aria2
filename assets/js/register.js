/* 
 * Aria is an Open Source project.
 */

$(document).ready(function () {
    $('#register').on('submit', function (e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var $this = $(this);

        var user = $('[name="user"]').val();
        var password = $('[name="password"]').val();
        var chpassword = $('[name="chpassword"]').val();
        var password = $('[name="password"]').val();
        var email = $('[name="email"]').val();
        var fname = $('[name="fname"]').val();
        var lname = $('[name="lname"]').val();
        var token_input = $('[name="token_inscription"]');
        var sbm = $('[name="sbm"]');

        if (password === '' || user === '' || chpassword === '' || email === '' || fname === '' || lname === '') {
            $.notify("Please fill all inputs.", "warn");
        } else {
            if (chpassword === password) {
                sbm.attr('disabled', 'disabled');
                $.ajax({
                    url: "auth.php",
                    type: $this.attr('method'),
                    data: $this.serialize(),
                    dataType: 'json', // JSON
                    success: function (json) {
                        if (json.registered === 'yes') {
                            $.notify("You are successfuly registered, welcome! I am redirecting you " + json.first_name + ".", "success");
                            var delay = 1000;
                            setTimeout(function () {
                                window.location = "me/";
                            }, delay);
                            sbm.html("Redirecting you...");
                        } else {
                            $.notify(json.infos, "error");
                            token_input.val(json.new_token);
                            sbm.removeAttr('disabled');
                        }
                    },
                    error: function () {
                        $.notify("Impossible to connect.", "error");
                        sbm.removeAttr('disabled');
                    }
                });
            } else {
                $.notify("Passwords not matching.", "warn");
            }
        }
    });
});