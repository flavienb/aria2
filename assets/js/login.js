/* 
 * Aria is an Open Source project.
 */

$(document).ready(function () {
    $('#login').on('submit', function (e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var $this = $(this);

        var user = $('[name="user"]').val();
        var password = $('[name="password"]').val();
        var token_input = $('[name="token_login"]');
        var sbm = $('[name="sbm"]');

        if (password === '' || user === '') {
            $.notify("Please fill all inputs.", "warn");
        } else {
            sbm.attr('disabled', 'disabled');
            $.ajax({
                url: "auth.php",
                type: $this.attr('method'),
                data: $this.serialize(),
                dataType: 'json', // JSON
                success: function (json) {
                    if (json.authentified === 'yes') {
                        $.notify("You are successfuly connected. I am redirecting you " + json.first_name + ".", "success");
                        var delay = 1000;
                        setTimeout(function () {
                            window.location = "me/";
                        }, delay);
                        sbm.html("Redirecting you...");
                    } else {
                        $.notify(json.infos, "error");
                        sbm.removeAttr('disabled');
                    }
                    token_input.val(json.new_token);
                },
                error: function () {
                    $.notify("Impossible to connect.", "error");
                    sbm.removeAttr('disabled');
                }
            });
        }
    });
});