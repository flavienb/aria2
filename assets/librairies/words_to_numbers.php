<?php

/*
 * Aria is an Open Source project.
 */

class WordsToNumbers {

    private $language = "";

    public function __construct($lang = "") {
        $this->language=$lang;
    }

    private $numbers_en = array(
        'zero' => 0,
        'one' => 1,
        'two' => 2,
        'three' => 3,
        'four' => 4,
        'five' => 5,
        'six' => 6,
        'seven' => 7,
        'eight' => 8,
        'nine' => 9,
        'ten' => 10,
        'eleven' => 11,
        'twelve' => 12,
        'thirteen' => 13,
        'fourteen' => 14,
        'fifteen' => 15,
        'sixteen' => 16,
        'seventeen' => 17,
        'eighteen' => 18,
        'nineteen' => 19,
        'twenty' => 20,
        'thirty' => 30,
        'forty' => 40,
        'fourty' => 40, // common misspelling
        'fifty' => 50,
        'sixty' => 60,
        'seventy' => 70,
        'eighty' => 80,
        'ninety' => 90,
        'hundred' => 100,
        'hundreds' => 100,
        'thousand' => 1000,
        "thousands" => 1000,
        'million' => 1000000,
        'millions' => 1000000,
        'billion' => 1000000000,
        'billions' => 1000000000,
        "and" => "and");
    private $numbers_fr = array(
        'zero' => 0,
        'un' => 1,
        'deux' => 2,
        'trois' => 3,
        'quatre' => 4,
        'cinq' => 5,
        'six' => 6,
        'sept' => 7,
        'huit' => 8,
        'neuf' => 9,
        'dix' => 10,
        'onze' => 11,
        'douze' => 12,
        'treize' => 13,
        'quatorze' => 14,
        'quinze' => 15,
        'seize' => 16,
        'dix-sept' => 17,
        'dix-huit' => 18,
        'dix-neuf' => 19,
        'vingt' => 20,
        'trente' => 30,
        'quarante' => 40, // common misspelling
        'cinquante' => 50,
        'soixante' => 60,
        'soixante-dix' => 70,
        'quatre-vingt' => 80,
        'quatre-vingt-dix' => 90,
        'cent' => 100,
        'cents' => 100,
        'mille' => 1000,
        "milles" => 1000,
        'million' => 1000000,
        'millions' => 1000000,
        'milliard' => 1000000000,
        'milliards' => 1000000000,
        "and" => "et"
    );

    public function convert($str) {
        if (!is_numeric($str)) {
            if ($this->language == "fr") {
                $numbers = $this->numbers_fr;
            } else {
                $numbers = $this->numbers_en;
            }

            $str = preg_replace("/[^a-zA-Z]+/", " ", $str);
            $words = explode(" ", $str);
            $total = 1;
            $force_addition = false;
            $last_digit = null;
            $final_sum = array();

            foreach ($words as $word) {
                if (!isset($numbers[$word]) && $word != $numbers["and"]) {
                    continue;
                }
                $word = strtolower($word);
                if ($word == "and") {
                    if ($last_digit === null) {
                        $total = 0;
                    }
                    $force_addition = true;
                } else {
                    if ($force_addition) {
                        $total += $numbers[$word];
                        $force_addition = false;
                    } else {
                        if ($last_digit !== null && $last_digit > $numbers[$word]) {
                            $total += $numbers[$word];
                        } else {
                            $total *= $numbers[$word];
                        }
                    }
                    $last_digit = $numbers[$word];
                    if ($numbers[$word] >= 1000) {
                        $final_sum[] = $total;
                        $last_digit = null;
                        $force_addition = false;
                        $total = 1;
                    }
                }
            }

            $final_sum[] = $total;
            return array_sum($final_sum);
        } else {
            return $str;
        }
    }

}

?>