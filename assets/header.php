<?php
session_start();
date_default_timezone_set("Europe/Paris");
require("functions.php");
?>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo correctPathFichier("assets/css/style.css") ?>"/>
        <link rel="icon" href="https://speeload.com/uploads/AtPHIgnhQR.PNG"/>
        <meta name="description" content="Aria is your very own open-source personal assistant, embeds vocal recognition and helps you be more productive.">
        <title>Aria - Your personal assistant - Login page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <script src="<?php echo correctPathFichier("assets/js/micspeech.js"); ?>"></script>
    </head>
    <body>
        <nav class="navbar navbar-light bg-faded">
            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#navbar-header" aria-controls="navbar-header" aria-expanded="false" aria-label="Toggle navigation"></button>
            <div class="collapse navbar-toggleable-xs" id="navbar-header">
                <a class="navbar-brand" href="#">Aria 2.</a>
                <ul class="nav navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= correctPathFichier("README.md"); ?>">Updates</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="https://gitlab.com/flavienb/aria2">GitLab</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">About</a>
                    </li>
                    <?php
                    if (isset($_SESSION["user"])) {
                        ?>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?= correctPathFichier("logout.php") ?>">Logout</a>
                        </li>
                        <?php
                    }
                    ?>
                </ul> 
            </div>
        </nav> <!-- /navbar -->

        <?php
        
        ?>