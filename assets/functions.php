<?php

// Gathers the functions accessible from anywhere.
include("librairies/words_to_numbers.php");

$time_zone = getTimeZone(getIp());
if (empty($time_zone)) {
    $time_zone = "Europe/Paris";
}
date_default_timezone_set($time_zone);

function getTimeZone($ip) {
    include(correctPathFichier("assets/api/api_external_keys.php"));
    $res = curlGet("ipinfo.io/" . $ip . "/json");
    $res = json_decode($res["response"], true);
    $position = $res["loc"];
    $res = curlGet("https://maps.googleapis.com/maps/api/timezone/json?location=" . $position . "&timestamp=" . time() . "&key=" . $apiKey_google);
    $res = json_decode($res["response"], true);
    if (isset($res["timeZoneId"])) {
        return $res["timeZoneId"];
    } else {
        return "Europe/Paris";
    }
}

function getIp() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    if ($ip == "::1") {
        $ip = "212.27.63.138"; //Default IP (Paris)
    }
    return $ip;
}

function is_between($val, $min, $max) {
    return ($val >= $min && $val <= $max);
}

function build_table($array) {
    $html = "";
    foreach ($array as $key => $value) {
        $html .= '<tr>';
        $html .= '<th>' . ucfirst($key) . '</th>';
        $html .= '<td>' . $value . '</td>';
        $html .= '<tr>';
    }
    return $html;
}

function correctPathFichier($path) {
    $open = $path;
    $open1 = "./" . $path;
    $open2 = "../" . $path;
    $open3 = "./../" . $path;
    $open4 = "../../" . $path;
    if (file_exists($open)) {
        return $path;
    } else if (file_exists($open1)) {
        $path = $open1;
        return $path;
    } else if (file_exists($open2)) {
        $path = $open2;
        return $path;
    } else if (file_exists($open3)) {
        $path = $open3;
        return $path;
    } else if (file_exists($open4)) {
        $path = $open4;
        return $path;
    } else {
        return "Introuvable";
    }
}

function http_file_exists($url, $followRedirects = true) {
    $url_parsed = parse_url($url);
    extract($url_parsed);
    if (!@$scheme)
        $url_parsed = parse_url('http://' . $url);
    extract($url_parsed);
    if (!@$port)
        $port = 80;
    if (!@$path)
        $path = '/';
    if (@$query)
        $path .= '?' . $query;
    $out = "HEAD $path HTTP/1.0\r\n";
    $out .= "Host: $host\r\n";
    $out .= "Connection: Close\r\n\r\n";
    if (!$fp = @fsockopen($host, $port, $es, $en, 5)) {
        return false;
    }
    fwrite($fp, $out);
    while (!feof($fp)) {
        $s = fgets($fp, 128);
        if (($followRedirects) && (preg_match('/^Location:/i', $s) != false)) {
            fclose($fp);
            return http_file_exists(trim(preg_replace("/Location:/i", "", $s)));
        }
        if (preg_match('/^HTTP(.*?)200/i', $s)) {
            fclose($fp);
            return true;
        }
    }
    fclose($fp);
    return false;
}

function isValidName($str) {
    if (preg_match('/^[a-zA-Z \'\-\.]{2,}+$/', $str)) {
        return true;
    } else {
        return false;
    }
}

function isValidPseudo($str) {
    if (preg_match('/^[a-zA-Z]{1,30}\d{0,4}$/', $str)) {
        return true;
    } else {
        return false;
    }
}

function isValidEmail($email) {
    $isValid = true;
    $atIndex = strrpos($email, "@");
    if (is_bool($atIndex) && !$atIndex) {
        $isValid = false;
    } else {
        $domain = substr($email, $atIndex + 1);
        $local = substr($email, 0, $atIndex);
        $localLen = strlen($local);
        $domainLen = strlen($domain);
        if ($localLen < 1 || $localLen > 64) {
            // local part length exceeded
            $isValid = false;
        } else if ($domainLen < 1 || $domainLen > 255) {
            // domain part length exceeded
            $isValid = false;
        } else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
            // local part starts or ends with '.'
            $isValid = false;
        } else if (preg_match('/\\.\\./', $local)) {
            // local part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
            // character not valid in domain part
            $isValid = false;
        } else if (preg_match('/\\.\\./', $domain)) {
            // domain part has two consecutive dots
            $isValid = false;
        } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
            // character not valid in local part unless
            // local part is quoted
            if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
                $isValid = false;
            }
        }
        if ($isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
            // domain not found in DNS
            $isValid = false;
        }
    }
    return $isValid;
}

function getDataFromJson($file) {
    if (file_exists($file)) {
        $data = file_get_contents($file);
        $data = json_decode($data, true);
        if (!is_array($data)) {
            $data = array();
        }
        return $data;
    } else {
        return false;
    }
}

function addDataJson($data, $file, $overwrite_file = false) {
    if ($overwrite_file === false) {
        $a_file = file_get_contents($file);
        if (empty($a_file)) {
            if (is_array($data) === false) {
                $data = array(0 => $data);
            }
            $a_data = $data;
        } else {
            $a_data = json_decode($a_file, true);
            if (is_array($data) === false) {
                array_push($a_data, $data);
            } else {
                if (isset($a_data[0])) {
                    array_push($a_data, $data);
                } else {
                    $a_data = array_merge($a_data, $data);
                }
            }
        }
        $to_put = $a_data;
    } else {
        $to_put = array($data);
    }
    $to_put = json_encode($to_put);
    return file_put_contents($file, $to_put);
}

function getUserInfosForModule($module_name, $username) {
    $data = getDataFromJson("../assets/data/users.json");
    if (isset($data[$username]["options"][$module_name])) {
        return $data[$username]["options"][$module_name];
    } else {
        return false;
    }
}

function setUserInfosForModule($module_name, $username, $to_put) {
    $data = getDataFromJson("../assets/data/users.json");
    $data[$username]["options"][$module_name] = $to_put;
    file_put_contents("../assets/data/users.json", json_encode($data));
}

function curlPost($adress, $post, $custom_header = false) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $adress);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    if ($custom_header) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $custom_header);
    }
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
    $r = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $array = ["response" => $r, "debug" => curl_error($curl), "status" => $code];
    curl_close($curl);
    return $array;
}

function curlGet($adress, $custom_header = false) {
    if (is_array($custom_header) === false) {
        $custom_header = array();
    }
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $adress,
        CURLOPT_HTTPHEADER => $custom_header
    ));
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $resp = curl_exec($curl);
    $array = ["response" => $resp, "debug" => curl_error($curl), "status" => $code];
    curl_close($curl);
    return $array;
}

function getHtmlElement($site, $element) {
    include("librairies/simple_html_dom.php");
    $html = file_get_html($site);
    if (!empty($html)) {
        $resultat = "";
        $ret = $html->find($element);
        foreach ($ret as $r) {
            $resultat .= "</br>" . utf8_encode($r);
        }
    } else {
        $resultat = false;
    }
    return $resultat;
}

function folder_exist($folder) {
    // Get canonicalized absolute pathname
    $path = realpath($folder);

    // If it exist, check if it's a directory
    if ($path !== false AND is_dir($path)) {
        // Return canonicalized absolute pathname
        return $path;
    }

    // Path/folder does not exist
    return false;
}

?>