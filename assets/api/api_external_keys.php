<?php

/*
 * Aria is an Open Source project.
 * All API keys are hidden for security reasons when uploaded on Git.
 */

/* API Key of Mashape */
$apiKey_mashape = "";
$array_mashape = array(
    "X-Mashape-Key: $apiKey_mashape",
    "Accept: application/json"
);

/* API Key of Google */
$apiKey_google ="";

/* API key of api.berwick.fr */
$apiKey_berwick="";