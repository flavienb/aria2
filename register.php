<?php
require("assets/header.php");
if (isset($_SESSION["user"])) {
    header("Location:me/");
}
$token = md5(time());
$_SESSION["token_inscription"] = md5($token . getIp());
?>
<div class="jumbotron vertical-center">
    <div class="container">
        <form method="POST" action="" id="register">
            <div class="col-lg-12">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="user">Your username :</label>
                        <input type="text" name="user" class="form-control" aria-describedby="userHelp" pattern="^[a-zA-Z]{1,30}\d{0,4}$"/>
                        <small id="userHelp" class="form-text text-muted">Please choose a username.</small>
                    </div>
                    <div class="form-group">
                        <label for="password">Your password :</label>
                        <input type="password" name="password" class="form-control" aria-describedby="passwordHelp"/>
                        <small id="passwordHelp" class="form-text text-muted">Please enter your password.</small>
                    </div>
                    <div class="form-group">
                        <label for="chpassword">Password verification :</label>
                        <input type="password" name="chpassword" class="form-control" aria-describedby="chpasswordHelp"/>
                        <small id="chpasswordHelp" class="form-text text-muted">Please enter your password again.</small>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="email">Your e-mail adress :</label>
                        <input type="email" name="email" class="form-control" aria-describedby="emailHelp"/>
                        <small id="emailHelp" class="form-text text-muted">Only used for critical communication.</small>
                    </div>
                    <div class="form-group">
                        <label for="fname">First name :</label>
                        <input type="text" name="fname" class="form-control" aria-describedby="fnameHelp" pattern="^[a-zA-Z \'\-\.]{2,}+$"/>
                        <small id="fnameHelp" class="form-text text-muted">Please enter your first name.</small>
                    </div>
                    <div class="form-group">
                        <label for="lname">Last name :</label>
                        <input type="text" name="lname" class="form-control" aria-describedby="lnameHelp" pattern="^[a-zA-Z \'\-\.]{2,}+$"/>
                        <small id="lnameHelp" class="form-text text-muted">Please enter your last name.</small>
                    </div>
                </div>
                <div class="form-group" style="float:right">
                    <p>All inputs are required.</p>
                    <button type="submit" class="btn btn-primary" name="sbm">Register</button>
                    <br/>
                </div>
                <a href="index.php">Already registered ?</a>
            </div>
            <input type="hidden" name="token_inscription" value="<?php echo $token; ?>"/>
        </form>
    </div>
</div>
<?php
$more = "<script src='assets/js/register.js'></script>";
require("assets/footer.php");
?>