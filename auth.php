<?php

// Manage the login and register process.
/*
 * Aria is an Open Source project.
 */

session_start();
include("assets/functions.php");
$users_file="assets/data/users.json";

$new_token = md5(time());
$data = array("authentified" => "no", "infos" => "Impossible to log you.");
if (true) { //IsAllowedToConnect() will be added later.
    $data["is_allowed_to_connect"] = "yes";
    if (isset($_POST["token_login"]) && isset($_POST["user"]) && isset($_POST["password"])) {
        $user = strtolower(htmlentities($_POST["user"]));
        $password = htmlentities($_POST["password"]);
        $password = hash("sha256", $password);
        $token = htmlentities($_POST["token_login"]);
        $token_sess = (isset($_SESSION["token_login"]))?$_SESSION["token_login"]:"";
        if (md5($token . getIp()) == $token_sess) {
            $users = getDataFromJson($users_file);
            if (isset($users[$user])) {
                $arr_user = $users[$user];
                if ($arr_user["password"] == $password) {
                    $_SESSION["user"] = $arr_user;
                    $_SESSION["user"]["username"] = $user;
                    $data["first_name"] = $arr_user["first_name"];
                    $data["authentified"] = "yes";
                    $datas["infos"] = "Successfuly connected. Redirecting you.";
                } else {
                    $data["infos"] = "Wrong credentials.";
                }
            } else {
                $data["infos"] = "Wrong credentials.";
            }
        } else {
            $data["infos"] = "Wrong token.";
        }
        $_SESSION["token_login"] = md5($new_token . getIp());
    } else if (isset($_POST["token_inscription"]) && isset($_POST["user"]) && isset($_POST["password"]) && isset($_POST["chpassword"]) && isset($_POST["email"]) && isset($_POST["fname"]) && isset($_POST["lname"])) {
        $user = strtolower(htmlentities($_POST["user"]));
        $email = strtolower(htmlentities($_POST["email"]));
        $password = htmlentities($_POST["password"]);
        $password = hash("sha256", $password);
        $chpassword = htmlentities($_POST["chpassword"]);
        $chpassword = hash("sha256", $chpassword);
        $token = htmlentities($_POST["token_inscription"]);
        $token_sess = $_SESSION["token_inscription"];
        $fname = strtolower(htmlentities($_POST["fname"]));
        $lname = strtolower(htmlentities($_POST["lname"]));
        $fname = ucfirst($fname);
        $lname = ucfirst($lname);
        if (md5($token . getIp()) == $token_sess) {
            if ($password == $chpassword) {
                if (isValidPseudo($user)) {
                    if (isValidName($fname . " " . $lname)) {
                        if (isValidEmail($email)) {
                            $users = getDataFromJson($users_file);
                            if (!isset($users[$user])) {
                                if (empty($users)) {
                                    $new_array = array();
                                } else {
                                    $new_array = $users;
                                }
                                $api_key=sha1($password . "aria");
                                $user_array = array(
                                    "password" => $password,
                                    "email" => $email,
                                    "first_name" => $fname,
                                    "last_name" => $lname,
                                    "age" => "",
                                    "birth_date" => "",
                                    "api_key" => $api_key,
                                    "options" => ""
                                );
                                $fpc = addDataJson(array($user => $user_array), $users_file);
                                addDataJson($api_key, "assets/api/api_keys.json");
                                if ($fpc) {
                                    $_SESSION["user"] = $user_array;
                                    $_SESSION["user"]["username"] = $user;
                                    $data["registered"] = "yes";
                                    $data["first_name"] = $fname;
                                    $data["infos"] = "Success.";
                                } else {
                                    $data["infos"] = "Impossible to create your account. Please try again.";
                                }
                            } else {
                                $data["infos"] = "Username already used, please choose another one.";
                            }
                        } else {
                            $data["infos"] = "Your email adress is invalid. Please choose another one.";
                        }
                    } else {
                        $data["infos"] = "Incorrect name. Please insert a correct name.";
                    }
                } else {
                    $data["infos"] = "Invalid username, please choose a correct pseudo.";
                }
            } else {
                $data["infos"] = "Passwords not matching.";
            }
        } else {
            $data["infos"] = "Wrong token.";
        }

        $_SESSION["token_inscription"] = md5($new_token . getIp());
    } else {
        $data["infos"] = "Inputs missing.";
        $_SESSION["token_login"] = md5($new_token . getIp());
        $_SESSION["token_inscription"] = md5($new_token . getIp());
    }
    $data["new_token"] = $new_token;
} else {
    $data["is_allowed_to_connect"] = "no";
    $data["infos"] = "You are not allowed to connect. Please wait five minutes.";
}

echo json_encode($data);
?>